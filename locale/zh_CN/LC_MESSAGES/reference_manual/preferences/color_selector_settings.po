msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___preferences___color_selector_settings.pot\n"

#: ../../<generated>:1
msgid "Make Brush Color Bluer"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:1
msgid "The color selector settings in Krita."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Color Selector"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Color"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:16
msgid "Color Selector Settings"
msgstr "拾色器设置"

#: ../../reference_manual/preferences/color_selector_settings.rst:18
msgid ""
"These settings directly affect Advanced Color Selector Dockers and the same "
"dialog box appears when the user clicks the settings button in that docker "
"as well. They also affect certain hotkey actions."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:20
msgid ""
"This settings menu has a drop-down for Advanced Color Selector, and Color "
"Hotkeys."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:23
msgid "Advanced Color Selector"
msgstr "高级拾色器"

#: ../../reference_manual/preferences/color_selector_settings.rst:25
msgid ""
"These settings are described on the page for the :ref:"
"`advanced_color_selector_docker`."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:28
msgid "Color Hotkeys"
msgstr "颜色快捷键"

#: ../../reference_manual/preferences/color_selector_settings.rst:30
msgid "These allow you to set the steps for the following actions:"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:32
msgid "Make Brush Color Darker"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:33
msgid ""
"This is defaultly set to :kbd:`K` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:34
msgid "Make Brush Color Lighter"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:35
msgid ""
"This is defaultly set to :kbd:`L` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:36
msgid "Make Brush Color More Saturated"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:37
#: ../../reference_manual/preferences/color_selector_settings.rst:39
msgid "This is defaultly unset and uses the :guilabel:`saturation` steps."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:38
msgid "Make Brush Color More Desaturated"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:40
msgid "Shift Brushcolor Hue clockwise"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:41
#: ../../reference_manual/preferences/color_selector_settings.rst:43
msgid "This is defaultly unset and uses the :guilabel:`Hue` steps."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:42
msgid "Shift Brushcolor Hue counter-clockwise"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:44
msgid "Make Brush Color Redder"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:45
#: ../../reference_manual/preferences/color_selector_settings.rst:47
msgid "This is defaultly unset and uses the :guilabel:`Redder/Greener` steps."
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:46
msgid "Make Brush Color Greener"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:48
msgid "Make Brush Color Yellower"
msgstr ""

#: ../../reference_manual/preferences/color_selector_settings.rst:49
#: ../../reference_manual/preferences/color_selector_settings.rst:51
msgid "This is defaultly unset and uses the :guilabel:`Bluer/Yellower` steps."
msgstr ""
