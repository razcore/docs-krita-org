msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___colors___viewing_conditions.pot\n"

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_example_metamerism.png"
msgstr ""

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_01.svg"
msgstr ""

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mixup_ex1_02.png"
msgstr ""

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/White_point_mix_up_ex1_03.svg"
msgstr ""

#: ../../general_concepts/colors/viewing_conditions.rst:None
msgid ".. image:: images/color_category/Krita_metamerism_presentation.svg"
msgstr ""

#: ../../general_concepts/colors/viewing_conditions.rst:1
msgid "What are viewing conditions."
msgstr "介绍观察条件。"

#: ../../general_concepts/colors/viewing_conditions.rst:10
#: ../../general_concepts/colors/viewing_conditions.rst:15
msgid "Viewing Conditions"
msgstr "观察条件"

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Metamerism"
msgstr ""

#: ../../general_concepts/colors/viewing_conditions.rst:10
msgid "Color"
msgstr ""

#: ../../general_concepts/colors/viewing_conditions.rst:17
msgid ""
"We mentioned viewing conditions before, but what does this have to do with "
"'white points'?"
msgstr "我们在前文曾经简单介绍过观察条件，但它与“白点”又有什么关系呢？"

#: ../../general_concepts/colors/viewing_conditions.rst:19
msgid ""
"A lot actually, rather, white points describe a type of viewing condition."
msgstr "答案是：两者的关系非常密切。白点实际上就是在描述一种观察条件。"

#: ../../general_concepts/colors/viewing_conditions.rst:21
msgid ""
"So, usually what we mean by viewing conditions is the lighting and "
"decoration of the room that you are viewing the image in. Our eyes try to "
"make sense of both the colors that you are looking at actively (the colors "
"of the image) and the colors you aren't looking at actively (the colors of "
"the room), which means that both sets of colors affect how the image looks."
msgstr ""
"我们通常所说的观察条件指的是我们观看图像时所在房间的照明和装修情况。我们的眼"
"睛会试图同时理解我们正在注意的颜色 (图像的颜色) 和那些我们并未注意的颜色 (房"
"间的颜色)。这意味着两组颜色都会影响该图像的视觉效果。"

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ".. image:: images/color_category/Meisje_met_de_parel_viewing.png"
msgstr ""

#: ../../general_concepts/colors/viewing_conditions.rst:27
msgid ""
"**Left**: Let's ruin Vermeer by putting a bright purple background that asks "
"for more attention than the famous painting itself. **Center**: a much more "
"neutral backdrop that an interior decorator would hate but brings out the "
"colors. **Right**: The approximate color that this painting is displayed "
"against in real life in the Maurits House, at the least, last time I was "
"there. Original image from wikipedia commons."
msgstr ""
"**左图** ：让我们在维米尔的大作外面套上一圈比画面本身更显眼的颜色来糟蹋大师的"
"手笔吧。 **中图** ：室内设计师所不屑的中性灰背景却有助于突出画面本身的颜色。 "
"**右图** ：本文作者参观莫瑞泰斯皇家美术馆时本作展区的大致背景色。原图来自 "
"Wikipedia Commons。"

#: ../../general_concepts/colors/viewing_conditions.rst:29
msgid ""
"This is for example, the reason why museum exhibitioners can get really "
"angry at the interior decorators when the walls of the museum are painted "
"bright red or blue, because this will drastically change the way how the "
"painting's colors look. (Which, if we are talking about a painter known for "
"their colors like Vermeer, could result in a really bad experience)."
msgstr ""
"看了上面的组图，我们应该不难理解为什么博物馆的布展人员会对那些把内墙粉刷成大"
"红大紫的室内设计师深恶痛绝，因为这样的室内环境会强烈地扭曲人们对参展作品色彩"
"的感受。尤其是当画家的用色风格与维米尔接近时，观展体验将会非常糟糕。"

#: ../../general_concepts/colors/viewing_conditions.rst:37
msgid ""
"Lighting is the other component of the viewing condition which can have "
"dramatic effects. Lighting in particular affects the way how all colors "
"look. For example, if you were to paint an image of sunflowers and poppies, "
"print that out, and shine a bright yellow light on it, the sunflowers would "
"become indistinguishable from the white background, and the poppies would "
"look orange. This is called `metamerism <https://en.wikipedia.org/wiki/"
"Metamerism_%28color%29>`_, and it's generally something you want to avoid in "
"your color management pipeline."
msgstr ""
"照明是观察条件的另一因素，它的影响可以非常强烈，对所有颜色的视觉效果造成冲"
"击。如果你画了一张向日葵和虞美人的画，然后把明亮的黄光打在画面上，向日葵和白"
"色的背景将混到一起，而虞美人的红色变成了橙色。这种现象叫做 `同色异谱 "
"<https://en.wikipedia.org/wiki/Metamerism_%28color%29>`_ ，我们应极力避免它发"
"生在色彩管理流程中。"

#: ../../general_concepts/colors/viewing_conditions.rst:39
msgid ""
"An example where metamerism could become a problem is when you start "
"matching colors from different sources together."
msgstr ""
"下面让我们通过具体的例子来学习，为什么条件同色异谱会在不同来源的颜色之间进行"
"比对时造成问题。"

#: ../../general_concepts/colors/viewing_conditions.rst:46
msgid ""
"For example, if you are designing a print for a red t-shirt that's not "
"bright red, but not super grayish red either. And you want to make sure the "
"colors of the print match the color of the t-shirt, so you make a dummy "
"background layer that is approximately that red, as correctly as you can "
"observe it, and paint on layers above that dummy layer. When you are done, "
"you hide this dummy layer and sent the image with a transparent background "
"to the press."
msgstr ""
"我们假设你在为一件红色衬衫设计转印图案。衬衫的红色不算鲜艳，但也说不上灰暗。"
"为了让转印图案的颜色在印到衣服上之后依然保持观感一致，你会把一种尽可能类似衣"
"服颜色的红色设为临时背景图层，然后在上面新建一层绘制图案。绘制完毕后你将临时"
"背景图层隐藏，然后把带有透明背景的图像发送给印刷公司。"

#: ../../general_concepts/colors/viewing_conditions.rst:54
msgid ""
"But when you get the t-shirt from the printer, you notice that all your "
"colors look off, mismatched, and maybe too yellowish (and when did that T-"
"Shirt become purple?)."
msgstr ""
"可当你拿到衬衫时却发现颜色有点不对劲，图案好像有点偏黄，而衬衫的颜色则变紫"
"了。"

#: ../../general_concepts/colors/viewing_conditions.rst:56
msgid "This is where white points come in."
msgstr "这就是白点在起作用。"

#: ../../general_concepts/colors/viewing_conditions.rst:58
msgid ""
"You probably observed the t-shirt in a white room where there were "
"incandescent lamps shining, because as a true artist, you started your work "
"in the middle of the night, as that is when the best art is made. However, "
"incandescent lamps have a black body temperature of roughly 2300-2800K, "
"which makes them give a yellowish light, officially called White Point A."
msgstr ""
"原来，你之前是在一个用白炽灯照明的白色房间观察衬衫的颜色的。而你这个夜猫子在"
"显示器面前挑灯夜战画出了这个图案。现在问题来了：白炽灯的色温只有 2300K 到 "
"2800K 左右，它们发出的光是偏黄的，被规定为白点 A。"

#: ../../general_concepts/colors/viewing_conditions.rst:61
msgid ""
"Your computer screen on the other hand, has a black body temperature of "
"6500K, also known as D65. Which is a far more blueish color of light than "
"the lamps you are hanging."
msgstr "而你的显示器的色温为 6500K，也被称作 D65。和白炽灯相比它要偏蓝许多。"

#: ../../general_concepts/colors/viewing_conditions.rst:63
msgid ""
"What's worse, Printers print on the basis of using a white point of D50, the "
"color of white paper under direct sunlight."
msgstr "更糟糕的是，打印机的基准白点是 D50，它也是白纸在阳光直射下的颜色。"

#: ../../general_concepts/colors/viewing_conditions.rst:70
msgid ""
"So, by eye-balling your t-shirt's color during the evening, you took its red "
"color as transformed by the yellowish light. Had you made your observation "
"in diffuse sunlight of an overcast (which is also roughly D65), or made it "
"in direct sunlight light and painted your picture with a profile set to D50, "
"the color would have been much closer, and thus your design would not be as "
"yellowish."
msgstr ""
"因此，白炽灯下面观察衬衫的颜色时，你感知的是被白炽灯的黄光扭曲过的红色。想要"
"制作的图案不再偏黄，你可以在阴天的室外光 (大致是 D65) 下观察衬衫颜色，也可以"
"在直射的阳光下面观察衬衫，然后在打印时选用 D50 的特性文件。这两种做法可以让打"
"印出来的结果会更加接近你的设计预想。"

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ".. image:: images/color_category/White_point_mixup_ex1_03.png"
msgstr ""

#: ../../general_concepts/colors/viewing_conditions.rst:77
msgid ""
"Applying a white balance filter will sort of match the colors to the tone as "
"in the middle, but you would have had a much better design had you designed "
"against the actual color to begin with."
msgstr ""
"通过应用白平衡滤镜，你可以把色调显示成中间的模样。但如果一开始就在准确的背景"
"色上制作图案的话，效果肯定会更好。"

#: ../../general_concepts/colors/viewing_conditions.rst:79
msgid ""
"Now, you could technically quickly fix this by using a white balancing "
"filter, like the ones in G'MIC, but because this error is caught at the end "
"of the production process, you basically limited your use of possible colors "
"when you were designing, which is a pity."
msgstr ""
"在技术上你可以用白平衡滤镜来进行补救，G'MIC 里面就有一个那样的滤镜。可因为这"
"个失误直到流程末尾才被发觉，所以你在设计阶段的选色就已经受到了不必要的限制，"
"这是很可惜的。"

#: ../../general_concepts/colors/viewing_conditions.rst:81
msgid ""
"Another example where metamerism messes things up is with screen projections."
msgstr "另外一个同色异谱造现象成负面影响的例子是屏幕投影。"

#: ../../general_concepts/colors/viewing_conditions.rst:83
msgid ""
"We have a presentation where we mark one type of item with red, another with "
"yellow and yet another with purple. On a computer the differences between "
"the colors are very obvious."
msgstr ""
"我们在下面这个演示文稿里插入了一个含有红、黄、紫的饼状图，在电脑屏幕上不同颜"
"色切片之间反差相当明显。"

#: ../../general_concepts/colors/viewing_conditions.rst:89
msgid ""
"However, when we start projecting, the lights of the room aren't dimmed, "
"which means that the tone scale of the colors becomes crunched, and yellow "
"becomes near indistinguishable from white. Furthermore, because the light in "
"the room is slightly yellowish, the purple is transformed into red, making "
"it indistinguishable from the red. Meaning that the graphic is difficult to "
"read."
msgstr ""
"可到了投影的时候，房间的灯光没有关掉，这一方面导致投影画面的颜色范围遭到挤"
"压，另一方面黄色的灯光染黄了幕布，导致黄和白之间难以分辨，而紫色也变成了红"
"色，导致红和紫之间也难以分辨。这样一来这个饼状图里面的几块区域就很难分清了。"

#: ../../general_concepts/colors/viewing_conditions.rst:91
msgid ""
"In both cases, you can use Krita's color management a little to help you, "
"but mostly, you just need to be ''aware'' of it, as Krita can hardly fix "
"that you are looking at colors at night, or the fact that the presentation "
"hall owner refuses to turn off the lights."
msgstr ""
"就这两个例子而言，你都可以通过 Krita 的色彩管理功能进行一些弥补，但更重要的还"
"是对观察条件的变化时刻保持警惕，毕竟再厉害的色彩管理也无法改变白炽灯对所见颜"
"色的扭曲，也无法替你关掉会议室的灯光。"

#: ../../general_concepts/colors/viewing_conditions.rst:93
msgid ""
"That said, unless you have a display profile that uses LUTs, such as an OCIO "
"LUT or a cLUT icc profile, white point won't matter much when choosing a "
"working space, due to weirdness in the icc v4 workflow which always converts "
"matrix profiles with relative colorimetric, meaning the white points are "
"matched up."
msgstr ""
"尽管如此，除非你的显示器特性文件使用的是 LUT，如 OCIO LUT 和 cLUT ICC 特性文"
"件，在选择工作空间时并不需要考虑白点数值。这是因为 ICC V4 色彩管理流程中的一"
"处古怪的规定，让阶调矩阵特性文件必须通过相对比色模式来进行转换，所以白点总是"
"匹配的。"
