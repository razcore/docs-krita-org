# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-19 23:14+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en icons Krita image multibrush toolmultibrush kbd\n"
"X-POFile-SpellExtra: freehandbrushtool multibrushtool mirrortools images\n"
"X-POFile-SpellExtra: alt ref guilabel mouseleft Kritamouseleft tools\n"
"X-POFile-SpellExtra: Mandala\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:40
msgid ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: toolmultibrush"
msgstr ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: ferramenta multi-pincéis"

#: ../../reference_manual/tools/multibrush.rst:None
msgid ".. image:: images/tools/Krita-multibrush.png"
msgstr ".. image:: images/tools/Krita-multibrush.png"

#: ../../reference_manual/tools/multibrush.rst:1
msgid "Krita's multibrush tool reference."
msgstr "A referência da ferramenta multi-pincéis do Krita."

#: ../../reference_manual/tools/multibrush.rst:11
#: ../../reference_manual/tools/multibrush.rst:31
msgid "Symmetry"
msgstr "Simetria"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Multibrush"
msgstr "Multi-Pincel"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Mandala"
msgstr "Mandala"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Rotational Symmetry"
msgstr "Simetria Rotacional"

#: ../../reference_manual/tools/multibrush.rst:16
msgid "Multibrush Tool"
msgstr "Ferramenta Multi-Pincel"

#: ../../reference_manual/tools/multibrush.rst:18
msgid "|toolmultibrush|"
msgstr "|toolmultibrush|"

#: ../../reference_manual/tools/multibrush.rst:20
msgid ""
"The Multibrush tool allows you to draw using multiple instances of a "
"freehand brush stroke at once, it can be accessed from the Toolbox docker or "
"with the default shortcut :kbd:`Q`. Using the Multibrush is similar to "
"toggling the :ref:`mirror_tools`, but the Multibrush is more sophisticated, "
"for example it can mirror freehand brush strokes along a rotated axis."
msgstr ""
"A ferramenta Multi-Pincéis permite-lhe desenhar várias instâncias de um "
"traço à mão-livre de cada vez, podendo ser acedida através da área de "
"Ferramentas ou com o atalho predefinido :kbd:`Q`. O uso dos Multi-Pincéis é "
"semelhante a comutar as :ref:`mirror_tools`, mas os Multi-Pincéis são mais "
"sofisticados - por exemplo, consegue fazer um espelho dos traços do pincel "
"ao longo de um eixo rodado."

#: ../../reference_manual/tools/multibrush.rst:22
msgid "The settings for the tool will be found in the tool options dock."
msgstr ""
"A configuração da ferramenta será encontrada na área de opções da ferramenta."

#: ../../reference_manual/tools/multibrush.rst:24
msgid ""
"The multibrush tool has three modes and the settings for each can be found "
"in the tool options dock. Symmetry and mirror reflect over an axis which can "
"be set in the tool options dock. The default axis is the center of the "
"canvas."
msgstr ""
"A ferramenta multi-pincéis tem três modos e a configuração de cada uma está "
"disponível na área de opções da ferramenta. A simetria e o espelho reflectem-"
"se ao longo de um eixo definido na área de opções da ferramenta. O eixo "
"predefinido é o centro da área de desenho."

#: ../../reference_manual/tools/multibrush.rst:29
msgid "The three modes are:"
msgstr "Os três modos são:"

#: ../../reference_manual/tools/multibrush.rst:32
msgid ""
"Symmetry will reflect your brush around the axis at even intervals. The "
"slider determines the number of instances which will be drawn on the canvas."
msgstr ""
"A simetria irá reflectir o seu pincel em torno do eixo em intervalos pares. "
"A barra define o número de instâncias que serão desenhadas na área de "
"desenho."

#: ../../reference_manual/tools/multibrush.rst:33
msgid "Mirror"
msgstr "Espelho"

#: ../../reference_manual/tools/multibrush.rst:34
msgid "Mirror will reflect the brush across the X axis, the Y axis, or both."
msgstr ""
"O espelho irá reflectir o pincel em torno do eixo dos X, dos Y ou em ambos."

#: ../../reference_manual/tools/multibrush.rst:35
msgid "Translate"
msgstr "Translação"

#: ../../reference_manual/tools/multibrush.rst:36
msgid ""
"Translate will paint the set number of instances around the cursor at the "
"radius distance."
msgstr ""
"A translação irá desenhar o número de instâncias configurado em torno do "
"cursor, com a distância definida pelo raio."

#: ../../reference_manual/tools/multibrush.rst:37
msgid "Snowflake"
msgstr "Floco de Neve"

#: ../../reference_manual/tools/multibrush.rst:38
msgid ""
"This works as a mirrored symmetry, but is a bit slower than symmetry+toolbar "
"mirror mode."
msgstr ""
"Isto funciona como uma simetria ao espelho, mas é um bocado mais lenta que o "
"modo de simetria+espelho na barra."

#: ../../reference_manual/tools/multibrush.rst:40
msgid "Copy Translate"
msgstr "Cópia e Translação"

#: ../../reference_manual/tools/multibrush.rst:40
msgid ""
"This allows you to set the position of the copies relative to your own "
"cursor. To set the position of the copies, first toggle :guilabel:`Add`, and "
"then |mouseleft| the canvas to place copies relative to the multibrush "
"origin. Finally, press :guilabel:`Add` again, and start drawing to see the "
"copy translate in action."
msgstr ""
"Isto permite-lhe definir a posição das cópias em relação ao seu próprio "
"cursor. Para definir a posição das cópias, primeiro active o :guilabel:"
"`Adicionar`, e depois use o |mouseleft| sobre a área de desenho para colocar "
"as cópias em relação à origem do multi-pincel. Finalmente, carregue em :"
"guilabel:`Adicionar` de novo, começando a desenhar para ver a cópia a entrar "
"em acção."

#: ../../reference_manual/tools/multibrush.rst:42
msgid ""
"The assistant and smoothing options work the same as in the :ref:"
"`freehand_brush_tool`, though only on the real brush and not its copies."
msgstr ""
"As opções do assistente e de suavização funcionam da mesma forma que o :ref:"
"`freehand_brush_tool`, ainda que só actuem no pincel real e não nas suas "
"cópias."
