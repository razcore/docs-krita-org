msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-08 11:58+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: icons Krita toolmeasure image measuretool images alt\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Kritamouseleft mouseleft\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:52
msgid ""
".. image:: images/icons/measure_tool.svg\n"
"   :alt: toolmeasure"
msgstr ""
".. image:: images/icons/measure_tool.svg\n"
"   :alt: ferramenta de medição"

#: ../../reference_manual/tools/measure.rst:1
msgid "Krita's measure tool reference."
msgstr "A referência à ferramenta de medição ou medida do Krita."

#: ../../reference_manual/tools/measure.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/measure.rst:11
msgid "Measure"
msgstr "Medida"

#: ../../reference_manual/tools/measure.rst:11
msgid "Angle"
msgstr "Ângulo"

#: ../../reference_manual/tools/measure.rst:11
msgid "Compass"
msgstr "Bússola"

#: ../../reference_manual/tools/measure.rst:16
msgid "Measure Tool"
msgstr "Ferramenta de Medida"

#: ../../reference_manual/tools/measure.rst:18
msgid "|toolmeasure|"
msgstr "|toolmeasure|"

#: ../../reference_manual/tools/measure.rst:20
msgid ""
"This tool is used to measure distances and angles. Click the |mouseleft| to "
"indicate the first endpoint or vertex of the angle, keep the button pressed, "
"drag to the second endpoint and release the button. The results will be "
"shown on the Tool Options docker. You can choose the length units from the "
"drop-down list."
msgstr ""
"Esta ferramenta é usada para medir distâncias e ângulos. Carregue no |"
"mouseleft| do rato para indicar o primeiro ponto final ou vértice do ângulo, "
"mantenha carregado o botão e arraste até ao segundo ponto final e largue o "
"botão. Os resultados serão apresentados na área de Opções da Ferramenta. "
"Poderá escolher as unidades de comprimento na lista respectiva."

#: ../../reference_manual/tools/measure.rst:23
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/measure.rst:25
msgid ""
"The measure tool-options allow you to change between the units used. Unit "
"conversion varies depending on the DPI setting of a document."
msgstr ""
"As opções da ferramenta de medição permitem-lhe alternar entre as várias "
"unidades usadas. A conversão de unidades varia de acordo com a configuração "
"de PPP de um documento."
