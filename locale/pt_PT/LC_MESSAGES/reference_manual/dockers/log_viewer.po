# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-05-24 15:42+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: GPU Krita guilabel\n"

#: ../../<generated>:1
msgid "Configure Logging"
msgstr "Configurar o Registo"

#: ../../reference_manual/dockers/log_viewer.rst:1
msgid "Overview of the log viewer docker."
msgstr "Introdução à área acoplável do visualizador de registos."

#: ../../reference_manual/dockers/log_viewer.rst:10
#: ../../reference_manual/dockers/log_viewer.rst:16
msgid "Log Viewer"
msgstr "Visualizador do Registo"

#: ../../reference_manual/dockers/log_viewer.rst:10
msgid "Debug"
msgstr "Depuração"

#: ../../reference_manual/dockers/log_viewer.rst:18
msgid ""
"The log viewer docker allows you to see debug output without access to a "
"terminal. This is useful when trying to get a tablet log or to figure out if "
"Krita is spitting out errors while a certain thing is happening."
msgstr ""
"A área do visualizador de registos permite-lhe ver o resultado de depuração "
"sem acesso a um terminal. Isto é útil quando estiver a tentar obter dados de "
"uma tablete ou a tentar descobrir se o Krita está a gerar erros enquanto "
"acontece algo."

#: ../../reference_manual/dockers/log_viewer.rst:20
msgid ""
"The log docker is used by pressing the :guilabel:`enable logging` button at "
"the bottom."
msgstr ""
"A área do registo é usada se carregar no botão :guilabel:`activar o registo` "
"no fundo."

#: ../../reference_manual/dockers/log_viewer.rst:24
msgid ""
"When enabling logging, this output will not show up in the terminal. If you "
"are missing debug output in the terminal, check that you didn't have the log "
"docker enabled."
msgstr ""
"Quando activar o registo, este resultado não irá aparecer no terminal. Se "
"lhe faltar alguns resultados de depuração no terminal, verifique se não "
"deixou a área de registo activada."

#: ../../reference_manual/dockers/log_viewer.rst:26
msgid ""
"The docker is composed of a log area which shows the debug output, and four "
"buttons at the bottom."
msgstr ""
"A área acoplável é composta por uma área de registo que mostra o resultado "
"de depuração no fundo."

#: ../../reference_manual/dockers/log_viewer.rst:29
msgid "Log Output Area"
msgstr "Área de Resultados do Registo"

#: ../../reference_manual/dockers/log_viewer.rst:31
msgid "The log output is formatted as follows:"
msgstr "O resultado do registo é formatado da seguinte forma:"

#: ../../reference_manual/dockers/log_viewer.rst:33
msgid "White"
msgstr "Branco"

#: ../../reference_manual/dockers/log_viewer.rst:34
msgid "This is just a regular debug message."
msgstr "Esta é apenas uma mensagem normal de depuração."

#: ../../reference_manual/dockers/log_viewer.rst:35
msgid "Yellow"
msgstr "Amarelo"

#: ../../reference_manual/dockers/log_viewer.rst:36
msgid "This is a info output."
msgstr "Esta é uma mensagem de informação."

#: ../../reference_manual/dockers/log_viewer.rst:37
msgid "Orange"
msgstr "Laranja"

#: ../../reference_manual/dockers/log_viewer.rst:38
msgid "This is a warning output."
msgstr "Este é um resultado de aviso."

#: ../../reference_manual/dockers/log_viewer.rst:40
msgid "Red"
msgstr "Vermelho"

#: ../../reference_manual/dockers/log_viewer.rst:40
msgid "This is a critical error. When this is bolded, it is a fatal error."
msgstr ""
"Este é um erro crítico. Quando estiver a negrito, trata-se de um erro fatal."

#: ../../reference_manual/dockers/log_viewer.rst:43
msgid "Options"
msgstr "Opções"

#: ../../reference_manual/dockers/log_viewer.rst:45
msgid "There's four buttons at the bottom:"
msgstr "Existem quatro botões no fundo:"

#: ../../reference_manual/dockers/log_viewer.rst:47
msgid "Enable Logging"
msgstr "Activar o Registo"

#: ../../reference_manual/dockers/log_viewer.rst:48
msgid "Enable the docker to start logging. This caries over between sessions."
msgstr ""
"Active a área para iniciar o registo. Isto passa de sessão para sessão."

#: ../../reference_manual/dockers/log_viewer.rst:49
msgid "Clear the Log"
msgstr "Limpar o Registo"

#: ../../reference_manual/dockers/log_viewer.rst:50
msgid "This empties the log output area."
msgstr "Isto limpa a área de resultados do registo."

#: ../../reference_manual/dockers/log_viewer.rst:51
msgid "Save the Log"
msgstr "Gravar o Registo"

#: ../../reference_manual/dockers/log_viewer.rst:52
msgid "Save the log to a text file."
msgstr "Grava o registo num ficheiro de texto."

#: ../../reference_manual/dockers/log_viewer.rst:54
msgid ""
"Configure which kind of debug is added. By default only warnings and simple "
"debug statements are logged. You can enable the special debug messages for "
"each area here."
msgstr ""
"Configura o tipo de depuração que é adicionado. Por omissão, só os avisos e "
"mensagens de depuração simples ficam registados. Poderá activar aqui as "
"mensagens de depuração especiais para cada área."

#: ../../reference_manual/dockers/log_viewer.rst:56
msgid "General"
msgstr "Geral"

#: ../../reference_manual/dockers/log_viewer.rst:57
msgid "Resource Management"
msgstr "Gestão de Recursos"

#: ../../reference_manual/dockers/log_viewer.rst:58
msgid "Image Core"
msgstr "Módulo-Base de Imagens"

#: ../../reference_manual/dockers/log_viewer.rst:59
msgid "Registries"
msgstr "Registos"

#: ../../reference_manual/dockers/log_viewer.rst:60
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/dockers/log_viewer.rst:61
msgid "Tile Engine"
msgstr "Motor de Padrões"

#: ../../reference_manual/dockers/log_viewer.rst:62
msgid "Filters"
msgstr "Filtros"

#: ../../reference_manual/dockers/log_viewer.rst:63
msgid "Plugin Management"
msgstr "Gestão de 'Plugins'"

#: ../../reference_manual/dockers/log_viewer.rst:64
msgid "User Interface"
msgstr "Interface do Utilizador"

#: ../../reference_manual/dockers/log_viewer.rst:65
msgid "File Loading and Saving"
msgstr "Carregamento e Gravação de Ficheiros"

#: ../../reference_manual/dockers/log_viewer.rst:66
msgid "Mathematics and Calculations"
msgstr "Matemática e Cálculos"

#: ../../reference_manual/dockers/log_viewer.rst:67
msgid "Image Rendering"
msgstr "Desenho da Imagem"

#: ../../reference_manual/dockers/log_viewer.rst:68
msgid "Scripting"
msgstr "Programação"

#: ../../reference_manual/dockers/log_viewer.rst:69
msgid "Input Handling"
msgstr "Tratamento de Dados de Entrada"

#: ../../reference_manual/dockers/log_viewer.rst:70
msgid "Actions"
msgstr "Acções"

#: ../../reference_manual/dockers/log_viewer.rst:71
msgid "Tablet Handing"
msgstr "Tratamento da Tablete"

#: ../../reference_manual/dockers/log_viewer.rst:72
msgid "GPU Canvas"
msgstr "Área de Desenho do GPU"

#: ../../reference_manual/dockers/log_viewer.rst:73
msgid "Metadata"
msgstr "Meta-dados"

#: ../../reference_manual/dockers/log_viewer.rst:74
msgid "Color Management"
msgstr "Gestão de Cores"
