# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:09+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/layers_and_masks/group_layers.rst:1
msgid "How to use group layers in Krita."
msgstr "Hur man använder grupplager i Krita"

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Layers"
msgstr "Lager"

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Groups"
msgstr "Grupper"

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Passthrough Mode"
msgstr "Genomskickningsläge"

#: ../../reference_manual/layers_and_masks/group_layers.rst:17
msgid "Group Layers"
msgstr "Grupplager"

#: ../../reference_manual/layers_and_masks/group_layers.rst:19
msgid ""
"While working in complex artwork you'll often find the need to group the "
"layers or some portions and elements of the artwork in one unit. Group "
"layers come in handy for this, they allow you to make a segregate the "
"layers, so you can hide these quickly, or so you can apply a mask to all the "
"layers inside this group as if they are one, you can also recursively "
"transform the content of the group... Just drag the mask so it moves to the "
"layer. They are quickly made with the :kbd:`Ctrl + G` shortcut."
msgstr ""
"Vid arbete med komplexa konstverk märker man ofta behovet att gruppera lager "
"eller vissa delar och element i konstverket som en enhet. Grupplager är "
"praktiska för detta, de tillåter att lagren skiljs åt så att de snabbt kan "
"döljas, eller att en mask kan appliceras för alla lager i gruppen som om de "
"vore ett enda. Det går också att transformera gruppens innehåll rekursivt. "
"Dra bara masken så att den flyttar lagret. De skapas snabbt med genvägen :"
"kbd:`Ctrl + G`."

#: ../../reference_manual/layers_and_masks/group_layers.rst:21
msgid ""
"A thing to note is that the layers inside a group layer are considered "
"separately when the layer gets composited, the layers inside a group are "
"separately composited and then this image is taken in to account when "
"compositing the whole image, while on the contrary, the groups in Photoshop "
"have something called pass-through mode which makes the layer behave as if "
"they are not in a group and get composited along with other layers of the "
"stack. The recent versions of Krita have pass-through mode you can enable it "
"to get similar behavior"
msgstr ""
"Något att observera är att lagren inuti ett grupplager anses vara separata "
"när lagret sammansätts, lagren inuti en grupp sammansätts separat och "
"därefter tas hänsyn till denna bild när hela bilden sammansätts, medan i "
"motsats till detta har grupper i Photoshop något som kallas genomgångsläge, "
"vilket gör att lagren beter sig som om de inte ingår i en grupp och "
"sammansätts med andra lager i traven. De senaste versionerna av Krita har "
"ett genomgångsläge som kan aktiveras för att få ett liknande beteende."
