# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:22+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/filters/colors.rst:None
msgid ".. image:: images/filters/Krita-color-to-alpha.png"
msgstr ".. image:: images/filters/Krita-color-to-alpha.png"

#: ../../reference_manual/filters/colors.rst:1
msgid "Overview of the color filters."
msgstr "Översikt av färgfiltren."

#: ../../reference_manual/filters/colors.rst:11
msgid "Filters"
msgstr "Filter"

#: ../../reference_manual/filters/colors.rst:16
msgid "Color"
msgstr "Färg"

#: ../../reference_manual/filters/colors.rst:18
msgid ""
"Similar to the Adjust filters, the color filters are image wide color "
"operations."
msgstr ""
"I liket med justeringsfiltren är färgfiltren färgoperationer över hela "
"bilden."

#: ../../reference_manual/filters/colors.rst:20
#: ../../reference_manual/filters/colors.rst:24
msgid "Color to Alpha"
msgstr "Färg till alfa"

#: ../../reference_manual/filters/colors.rst:26
msgid ""
"This filter allows you to make one single color transparent (alpha). By "
"default when you run this filter white is selected, you can choose a color "
"that you want to make transparent from the color selector."
msgstr ""
"Filtret låter dig gör en enskild färg genomskinlig (alfa). Normalt är vit "
"valt när filtret körs, men man kan välja en färg som man vill göra "
"genomskinlig i färgväljaren."

#: ../../reference_manual/filters/colors.rst:29
msgid ".. image:: images/filters/Color-to-alpha.png"
msgstr ".. image:: images/filters/Color-to-alpha.png"

#: ../../reference_manual/filters/colors.rst:30
msgid ""
"The Threshold indicates how much other colors will be considered mixture of "
"the removed color and non-removed colors. For example, with threshold set to "
"255, and the removed color set to white, a 50% gray will be considered a "
"mixture of black+white, and thus transformed in a 50% transparent black."
msgstr ""
"Tröskeln anger hur mycket andra färger anses vara en blandning av den "
"borttagna färgen och icke borttagna färger. Med tröskeln exempelvis inställt "
"till 255, och den borttagna färgen inställd till vit, anses 50 % grå vara en "
"blandning av svart+vit, och därigenom omvandlad till 50 % genomskinlig svart."

#: ../../reference_manual/filters/colors.rst:36
msgid ""
"This filter is really useful in separating line art from the white "
"background."
msgstr ""
"Filtret är mycket användbart för att skilja teckningar från den vita "
"bakgrunden."

#: ../../reference_manual/filters/colors.rst:41
msgid "Color Transfer"
msgstr "Färgöverföring"

#: ../../reference_manual/filters/colors.rst:43
msgid ""
"This filter converts the colors of the image to colors from the reference "
"image. This is a quick way to change a color combination of an artwork to an "
"already saved image or a reference image."
msgstr ""
"Filtret konverterar bildens färger till färger från referensbilden. Det är "
"ett snabbt sätt att ändra färgkombinationen hos ett alster till en redan "
"sparad bild eller en referensbild."

#: ../../reference_manual/filters/colors.rst:47
msgid ".. image:: images/filters/Color-transfer.png"
msgstr ".. image:: images/filters/Color-transfer.png"

#: ../../reference_manual/filters/colors.rst:51
msgid "Maximize Channel"
msgstr "Maximera kanal"

#: ../../reference_manual/filters/colors.rst:53
msgid ""
"This filter checks for all the channels of a each single color and set all "
"but the highest value to 0."
msgstr ""
"Filtret undersöker alla kanaler för varje enskild färg och ändrar alla utom "
"det högsta värdet till 0."

#: ../../reference_manual/filters/colors.rst:58
msgid "Minimize Channel"
msgstr "Minimera kanal"

#: ../../reference_manual/filters/colors.rst:60
msgid ""
"This is reverse to Maximize channel, it checks all the channels of a each "
"single color and sets all but the lowest to 0."
msgstr ""
"Omvändning av Maximera kanal. Det undersöker alla kanaler för varje enskild "
"färg och ändrar alla utom det lägsta till 0."
