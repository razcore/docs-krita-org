# Translation of docs_krita_org_tutorials___krita-brush-tips.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials___krita-brush-tips\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-23 11:41+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../tutorials/krita-brush-tips.rst:1
#: ../../tutorials/krita-brush-tips.rst:13
msgid ""
"Krita Brush-tips is an archive of brush-modification tutorials done by the "
"krita-foundation.tumblr.com account based on user requests."
msgstr ""
"«Кінчики пензлів» Krita (або Brush-tips англійською) — архів підручників "
"щодо модифікації пензлів, який створено на основі дописів у обліковому "
"записі krita-foundation.tumblr.com та відповідей на питання користувачів."

#: ../../tutorials/krita-brush-tips.rst:15
msgid "Topics:"
msgstr "Теми:"
