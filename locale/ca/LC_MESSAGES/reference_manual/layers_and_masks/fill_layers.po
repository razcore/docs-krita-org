# Translation of docs_krita_org_reference_manual___layers_and_masks___fill_layers.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 20:35+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:0
msgid ".. image:: images/layers/fill_layer_simplex_noise.png"
msgstr ".. image:: images/layers/fill_layer_simplex_noise.png"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:1
msgid "How to use fill layers in Krita."
msgstr "Com emprar les capes d'emplenat en el Krita."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Layers"
msgstr "Capes"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Fill"
msgstr "Emplenat"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Generator"
msgstr "Generador"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:17
msgid "Fill Layers"
msgstr "Capes d'emplenat"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:19
msgid ""
"A Fill Layer is a special layer that Krita generates on-the-fly that can "
"contain either a pattern or a solid color."
msgstr ""
"Una capa d'emplenat és una capa especial que el Krita generarà sobre la "
"marxa, la qual pot contenir un patró o un color sòlid."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:22
msgid ".. image:: images/layers/Fill_Layer.png"
msgstr ".. image:: images/layers/Fill_Layer.png"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:24
msgid ""
"This fills the layer with a predefined pattern or texture that has been "
"loaded into Krita through the Resource Management interface.  Patterns can "
"be a simple and interesting way to add texture to your drawing or painting, "
"helping to recreate the look of watercolor paper, linen, canvas, hardboard, "
"stone or an infinite other number of options.  For example if you want to "
"take a digital painting and finish it off with the appearance of it being on "
"canvas you can add a Fill Layer with the Canvas texture from the texture "
"pack below and set the opacity very low so the \"threads\" of the pattern "
"are just barley visible.  The effect is quite convincing."
msgstr ""
"Emplenarà la capa amb un patró o textura predefinits que s'han carregat en "
"el Krita a través de la interfície Gestió de recursos. Els patrons poden ser "
"una forma senzilla i interessant d'afegir textura al vostre dibuix o "
"pintura, ajudant a recrear l'aspecte del paper, lli, llenç, taulers durs, "
"pedra o un nombre infinit d'opcions per a l'aquarel·la. Per exemple, si "
"voleu prendre una pintura digital i finalitzar-la amb l'aparença que es "
"troba en el llenç, podreu afegir una Capa d'emplenat amb la textura Llenç "
"del paquet de textures i després establir l'opacitat molt baixa, de manera "
"que els «fils» del patró gairebé no seran visibles. L'efecte és força "
"convincent."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:26
msgid "Pattern"
msgstr "Patró"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:26
msgid ""
"You can create your own and use those as well.  For a great set of well "
"designed and useful patterns check out one of our favorite artists and a "
"great friend of Krita, David Revoy's free texture pack (https://www."
"davidrevoy.com/article156/texture-pack-1)."
msgstr ""
"També podreu crear els vostres i emprar-los. Per a un gran conjunt de "
"patrons ben dissenyats i útils, reviseu un dels nostres artistes favorits i "
"un gran amic del Krita, el paquet de textures lliure d'en David Revoy "
"(https://www.davidrevoy.com/article156/texture-pack-1)."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:29
msgid "Color"
msgstr "Color"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:29
msgid ""
"The second option is not quite as exciting, but does the job. Fill the layer "
"with a selected color."
msgstr ""
"La segona opció no és tan emocionant, però fa la feina. Emplena la capa amb "
"un color seleccionat."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:36
msgid ""
"A noise generator that isn't Perline Noise (which is what typical 'clouds' "
"generation is), but it looks similar and can actually loop. Uses the "
"OpenSimplex code."
msgstr ""
"Un generador de soroll que no és Soroll Perlin (el qual és la típica "
"generació de «núvols»), però s'hi assembla i en realitat pot fer un bucle. "
"Utilitza el codi d'OpenSimplex."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:38
msgid "Looping"
msgstr "Repetició"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:39
msgid "Whether or not to force the pattern to loop."
msgstr "Indica si forçar o no el patró per a la repetició."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:40
msgid "Frequency"
msgstr "Velocitat"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:41
msgid ""
"The frequency of the waves used to generate the pattern. Higher frequency "
"results in a finer noise pattern."
msgstr ""
"La freqüència de les ones utilitzades per a generar el patró. Una freqüència "
"més alta donarà com a resultat un patró de soroll més fi."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:42
msgid "Ratio"
msgstr "Relació"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:43
msgid ""
"The ratio of the waves in the x and y dimensions. This makes the noise have "
"a rectangular appearance."
msgstr ""
"La relació de les ones en les dimensions X i Y. Això farà que el soroll "
"tingui un aspecte rectangular."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid "Simplex Noise"
msgstr "Soroll símplex"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid "Use Custom Seed"
msgstr "Usa una llavor personalitzada"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid ""
"The seed for the random component. You can input any value or text here, and "
"it will always try to use this value to generate the random values with "
"(which then are always the same for a given seed)."
msgstr ""
"La llavor per al component aleatori. Aquí podreu introduir qualsevol valor o "
"text, i sempre s'intentarà utilitzar aquest valor per a generar els valors "
"aleatoris (els quals després sempre seran els mateixos per a una llavor "
"indicada)."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:48
msgid "Painting on a fill layer"
msgstr "Pintar sobre una capa d'emplenat"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:50
msgid ""
"A fill-layer is a single-channel layer, meaning it only has transparency. "
"Therefore, you can erase and paint on fill-layers to make them semi-opaque, "
"or for when you want to have a particular color only. Being single channel, "
"fill-layers are also a little bit less memory-consuming than regular 4-"
"channel paint layers."
msgstr ""
"Una capa d'emplenat és una capa d'un sol canal, el qual vol dir que només té "
"transparència. Per tant, podreu esborrar i pintar sobre les capes d'emplenat "
"per a fer-les semiopaques, o per a quan només vulgueu tenir un color en "
"particular. Com que són d'un sol canal, les capes d'emplenat també "
"consumeixen una mica menys de memòria que les capes de pintura normals amb 4 "
"canals."
